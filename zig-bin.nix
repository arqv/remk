{ pkgs ? import <nixpkgs> { } }:
version: shasums:
let
  arch = if pkgs.stdenv.isDarwin then "macos" else "linux";
  attrs = {
    url = "https://ziglang.org/download/${version}/zig-${arch}-x86_64-${version}.tar.xz";
    sha256 = shasums.${arch};
  };
  src = pkgs.fetchurl attrs;
in
pkgs.stdenvNoCC.mkDerivation {
  name = "zig-bin";
  inherit version src;
  dontConfigure = true;
  dontBuild = true;
  installPhase = ''
    mkdir -p $out $out/bin $out/doc
    mv lib $out/lib
    mv zig $out/bin/zig
    mv langref.html $out/doc/langref.html
  '';
  dontFixup = true; # binaries are compiled statically
}
