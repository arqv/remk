# remk
Work-in-progress reproducible build system.

**remk** uses Janet, a small embedded LISP language for defining build recipes.

```janet
(build (
	(file/add (fn/compose (fls/bake fls/sort fls/wildcard) "*.c"))) (
		(sh/$ touch (string/append (file/name $file) ".txt"))
	))
))
```

