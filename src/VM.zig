const std = @import("std");
const janet_c = @cImport(@cInclude("janet/janet.h"));

const Self = @This();

pub const Error = error{
    InitError,
    RuntimeError,
    OutOfMemory,
};

pub const ValueType = enum {
    number,
    nil,
    boolean,
    fiber,
    string,
    symbol,
    keyword,
    array,
    tuple,
    table,
    @"struct",
    buffer,
    function,
    cfunction,
    abstract,
    pointer,

    pub fn getType(comptime x: ValueType) type {
        return switch (x) {
            .number => f64,
            .nil => void,
            .boolean => bool,
            .fiber => *janet_c.JanetFiber,
            .string => []const u8,
            .symbol => []const u8,
            .keyword => []const u8,
        };
    }
};

pub const Value = struct {
    inner: janet_c.Janet,

    pub fn unwrap(self: *Value, comptime kind: ValueType) !ValueType.getType(kind) {
        
    }

    pub fn typeOf(self: *Value) ValueType {
        return @intToEnum(ValueType, @intCast(u4, @enumToInt(janet_c.janet_type(self.inner))));
    }

    pub fn makeNil() Value {
        return Value{ .inner = janet_c.janet_wrap_nil() };
    }
    pub fn makeNumber(value: f64) Value {
        return Value{ .inner = janet_c.janet_wrap_number(value) };
    }
    pub fn makeBool(value: bool) Value {
        return Value{
            .inner = if (value)
                janet_c.janet_wrap_true()
            else
                janet_c.janet_wrap_false(),
        };
    }
};

env: *janet_c.JanetTable,
allocator: *std.mem.Allocator,

pub fn init(alloc: *std.mem.Allocator, replacements: ?*janet_c.JanetTable) Error!Self {
    var code = janet_c.janet_init();
    if (code != 0) return Error.InitError;
    return Self{
        .allocator = alloc,
        .env = janet_c.janet_core_env(replacements),
    };
}
pub fn deinit(self: *Self) void {
    janet_c.janet_deinit();
}

pub fn runString(self: *Self, string: []const u8, src: []const u8) Error!*Value {
    var value: *Value = try self.allocator.create(Value);
    var code = janet_c.janet_dobytes(self.env, @ptrCast([*c]const u8, string), @truncate(i32, @intCast(isize, string.len)), @ptrCast([*c]const u8, src), &value.inner);
    if (code != 0) return Error.RuntimeError else return value;
}
