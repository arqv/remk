const std = @import("std");

var _gpa = std.heap.GeneralPurposeAllocator(.{}){};
const gpa = &_gpa.allocator;

const VM = @import("VM.zig"); 
 
pub fn main() anyerror!void {
    defer { _ = _gpa.deinit(); }

    var vm = try VM.init(gpa, null);
    defer vm.deinit();
    
    const program = "(print (+ 1.1 2.6))";

    var value = try vm.runString(program, "main");
    defer gpa.destroy(value);
}
