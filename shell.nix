{ pkgs ? import <nixpkgs> { } }:
let
  zig-bin = import ./zig-bin.nix { inherit pkgs; };
  zig = zig-bin "0.7.0" {
    macos = "94063f9a311cbbf7a2e0a12295e09437182cf950f18cb0eb30ea9893f3677f24";
    linux = "e619b1c6094c095b932767f527aee2507f847ea981513ff8a08aab0fd730e0ac";
  };
  zls = import
    (pkgs.fetchFromGitHub {
      owner = "zigtools";
      repo = "zls";
      rev = "72f811e8dc2c28165fbbc28e5efbb4f3616ffa67";
      sha256 = "nVKQO1iEHH6KgE48I4BH7Q3a89mpB9BF7U0f4/o/Mjo=";
      fetchSubmodules = true;
    })
    {
      inherit zig;
      extraConfig = {
        warn_style = true;
        enable_semantic_tokens = true;
      };
    };
in
pkgs.mkShell {
  buildInputs = [
    zig
    zls
    pkgs.bashInteractive
  ];
}
